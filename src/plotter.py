#!/usr/bin/env python
# license removed for brevity
import rospy
import math
from std_msgs.msg import Float64
from nav_msgs.msg import Odometry
import matplotlib.pyplot as plt
import numpy as np
import numpy.linalg as linalg

class Plotter(object):
    """docstring for ClassName"""
    def __init__(self):
        rospy.init_node('plotter', anonymous=True)
        rospy.Subscriber("/localizablity", Float64, self.localizablity_callback)
        # rospy.Subscriber("/lidar_ekf/imu_odom", Odometry, self.covariance_callback)
        # rospy.Subscriber("/state_estimation_error", Float64, self.pose_error_callback)
        rospy.Subscriber("/lidar_ekf/imu_odom", Odometry, self.pose_callback)
        rospy.Subscriber("/lidar_ekf/imu_odom_limited", Odometry, self.pose_estimate_callback)
        
        self.localizablity = -1;
        self.covariance_max_count = 100;
        self.covariance = [0] * self.covariance_max_count;
        self.covariance_counter = 0;
        self.pose_error = 0;
        rate = rospy.Rate(15) 

        plt.ion()

        fig, ax = plt.subplots()

        plot = ax.scatter([], [])
        ax.set_xlabel('Localizablity')
        ax.set_ylabel('Covariance')
        # ax.set_xlim(-5, 5)
        # ax.set_ylim(-5, 5)

        while(self.covariance_counter<(self.covariance_max_count-2)):
            rate.sleep()

        error = 0;
        while not rospy.is_shutdown():
            error = math.sqrt((self.gt_pose.x - self.est_pose.x)**2 + (self.gt_pose.y - self.est_pose.y)**2 + (self.gt_pose.z - self.est_pose.z)**2);
            array = plot.get_offsets()

            array = np.append(array, np.array([self.localizablity,sum(self.covariance)/self.covariance_max_count]))
            plot.set_offsets(array)

            # update x and ylim to show all points:
            ax.set_xlim(array[:, 0].min(), array[:,0].max())
            ax.set_ylim(array[:, 1].min(), array[:, 1].max())
            # update the figure
            fig.canvas.draw()


            rate.sleep()


    def localizablity_callback(self,data):
        # print(data)
        self.localizablity = data.data;

    # def covariance_callback(self,data):
    #     # print(data.twist.covariance[0],data.twist.covariance[7],data.twist.covariance[14])
    #     self.covariance[self.covariance_counter] = math.sqrt(math.sqrt(data.twist.covariance[0]**2 + data.twist.covariance[7]**2 + data.twist.covariance[14]**2))
    #     self.covariance_counter = (self.covariance_counter + 1) % self.covariance_max_count;

    def pose_error_callback(self,data):
        self.pose_error = data.data;
        
    def pose_callback(self,data):
        self.gt_pose = data.pose.pose.position

    def pose_estimate_callback(self,data):
        self.est_pose = data.pose.pose.position
        # self.covariance[self.covariance_counter] = math.sqrt(math.sqrt(data.twist.covariance[0]**2 + data.twist.covariance[7]**2 + data.twist.covariance[14]**2))
        test = np.matrix(data.twist.covariance)
        eigenValues,eigenVectors = linalg.eig(np.reshape(test,(6,6)))
        # print(eigenValues[0:3])
        self.covariance[self.covariance_counter] = max(eigenValues[0:3]);

        self.covariance_counter = (self.covariance_counter + 1) % self.covariance_max_count;

if __name__ == '__main__':
    try:
        p = Plotter()
    except rospy.ROSInterruptException:
        pass