/**
 * @file   localizability_evaluation.cpp
 * @author Sam Zeng
 * @date   11/17/2017
 *
 * @copyright
 * Copyright (C) 2017.
 */


#include "localizability/Calculate_Localizablity.h"
// #include <ros/package.h>
#include <iostream>
#include <gazebo_msgs/ModelState.h>
#include <gazebo_msgs/SetModelState.h>
#include <geometry_msgs/Pose.h>
#include <tf/transform_listener.h>

void reset_sim_pose(ros::ServiceClient client, float x, float y, float z, float theta){
	gazebo_msgs::SetModelState setmodelstate;
	gazebo_msgs::ModelState new_state = gazebo_msgs::ModelState();

	new_state.model_name = "dji";
	new_state.pose.position.x = x;
	new_state.pose.position.y = y;
	new_state.pose.position.z = z;
	setmodelstate.request.model_state = new_state;
	client.call(setmodelstate);
}

void reset_est_pose(ros::NodeHandle n, ros::Publisher reset_pub, float x, float y, float z, float theta, float offset){

	geometry_msgs::Pose pose_msg = geometry_msgs::Pose();

	pose_msg.position.x = x - offset;
	pose_msg.position.y = y; 
	pose_msg.position.z = z;

	n.setParam("initial_x", - x + offset);
	n.setParam("initial_y", y - offset);
	n.setParam("initial_z", z - offset);


	reset_pub.publish(pose_msg);
}

void measure_pose_error(std::ofstream* data_file){
	tf::TransformListener listener;
	float err_x;
	float err_y;
	float err_z;
	float mean_err_x = 0;
	float mean_err_y = 0;
	float mean_err_z = 0;
	float max_err_x = 0;
	float max_err_y = 0;
	float max_err_z = 0;
	float delta_err_x; 
	float delta_err_y; 
	float delta_err_z;
	float var_err_x = 0; 
	float var_err_y = 0; 
	float var_err_z = 0;



	float count = 0;
	ros::Rate r(10);
	
	float wait_time = 32.5;
	while(ros::Time::now().toSec() == 0){
		r.sleep();
	}
	float start = ros::Time::now().toSec();
	float finish = start+wait_time;

	while(ros::Time::now().toSec() < finish && ros::ok()){
		tf::StampedTransform transform;
		try{
			listener.lookupTransform("/coax","/hokuyo_joint_static",ros::Time(0),transform);
		}
		catch (tf::TransformException ex){
			if(count == 0){
				continue;
			}
			ROS_ERROR("%s",ex.what());
			ros::Duration(1.0).sleep();
			continue;
		}
		if(ros::Time::now().toSec()-start < 2.5){
			continue;
		}
		count++;
		err_x = transform.getOrigin().x();
		err_y = transform.getOrigin().y();
		err_z = transform.getOrigin().z();
		
		delta_err_x = (fabs(err_x) - mean_err_x);
		delta_err_y = (fabs(err_y) - mean_err_y);
		delta_err_z = (fabs(err_z) - mean_err_z);


		mean_err_x += delta_err_x/count;
		mean_err_y += delta_err_y/count;
		mean_err_z += delta_err_z/count;

		var_err_x += delta_err_x*(fabs(err_x)-mean_err_x);
		var_err_y += delta_err_y*(fabs(err_x)-mean_err_y);
		var_err_z += delta_err_z*(fabs(err_x)-mean_err_z);


		if(fabs(err_x)>max_err_x){
			max_err_x = fabs(err_x);
		}
		if(fabs(err_y)>max_err_y){
			max_err_y = fabs(err_y);
		}
		if(fabs(err_z)>max_err_z){
			max_err_z = fabs(err_z);
		}
		r.sleep();
	}
	if(count < 2){
		ROS_ERROR("NO POSE DATA");
		return;
	}
	var_err_x = var_err_x/(count-1);
	var_err_y = var_err_y/(count-1);
	var_err_z = var_err_z/(count-1);
	(*data_file) << err_x << "\t" << err_y << "\t" << err_z << "\t" << mean_err_x << "\t" << mean_err_y << "\t" << mean_err_z << "\t" << max_err_x << "\t" << max_err_y << "\t" << max_err_z << "\t" << var_err_x << "\t" << var_err_y << "\t" << var_err_z;
	std::cout << err_x << " " << err_y << " " << err_z << " " << mean_err_x << " " << mean_err_y << " " << mean_err_z << " " << max_err_x << " " << max_err_y << " " << max_err_z << std::endl;
}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "localizablity_evaluator");
  ros::NodeHandle n;

  ros::ServiceClient client = n.serviceClient<gazebo_msgs::SetModelState>("/gazebo/set_model_state");
  ros::Publisher reset_pub =  n.advertise<geometry_msgs::Pose>("/localization_reset", 100);
  float x_min;
  float x_max;
  float dx;

  float y_min;
  float y_max;
  float dy;
   
  float z_min;
  float z_max;
  float dz;

  float theta = 0;

  n.getParam("x_min", x_min);
  n.getParam("x_max", x_max);
  n.getParam("dx", dx);
  n.getParam("y_min", y_min);
  n.getParam("y_max", y_max);
  n.getParam("dy", dy);
  n.getParam("z_min", z_min);
  n.getParam("z_max", z_max);
  n.getParam("dz", dz);
  n.getParam("theta",theta);

  // subcriber for pose error

  Calculate_Localizablity c = Calculate_Localizablity(n);
  std::ofstream data_file;
  std::string path = ros::package::getPath("localizablity");
  data_file.open((path+"/data/localizability_evaluation.txt").c_str());

  int length = int(((x_max-x_min)/dx+.9999));
  int width = int(((y_max-y_min)/dy+.9999));
  int height = int(((z_max-z_min)/dz+.9999));

  float offset = .25;

  reset_sim_pose(client,0,0,0,theta);
  reset_est_pose(n,reset_pub,0,0,0,theta,offset);
  ros::Duration(2).sleep();


  for(float z = z_min; z < z_max; z+=dz)
  {
    for (float x = x_min; x < x_max; x+=dx)
    {
      for (float y = y_min; y < y_max; y+=dy)
      {
      // ROS_WARN("TESTING");
      if(c.check_neighborhood_occupancy(x,y,z)){
      	ROS_WARN("OCCUPIED POINT");
      	continue;
      }
      // ROS_WARN("TESTING2");
      float L = c.estimate_localizablity(x,y,z,theta);
      // std::cout << "LOCALIZABLITY: " << L << std::endl;
      
      reset_sim_pose(client,x,y,z,theta);
      reset_est_pose(n,reset_pub,x,y,z,theta,offset);
      // std::cin.ignore();
      data_file << x << "\t" << y << "\t" << z << "\t" << L << "\t";
      measure_pose_error(&data_file);
      data_file << std::endl;      
      }
    }
  }
  data_file.close();
  return 0;
}




// void reset_callback(ros::NodeHandle &nh, geometry_msgs::Pose p)