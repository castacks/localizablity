/**
 * @file   localizability_level_sets.cpp
 * @author Sam Zeng
 * @date   11/17/2017
 *
 * @copyright
 * Copyright (C) 2017.
 */

#include <localizablity/Interpolator.h>
#include <localizablity/Calculate_Localizablity.h>


int main(int argc,  char** argv)
{
  ros::init(argc, argv, "localizablity level sets");
  ros::NodeHandle n;

  std::string path_to_point_cloud;
  float level_set_threshold = 12;
  n.getParam("path_to_point_cloud", path_to_point_cloud);
  n.getParam("level_set_threshold", level_set_threshold);

  ros::Rate loop_rate(10);


  Interpolator I = Interpolator();

  pcl::PointCloud<pcl::PointXYZ>::Ptr boundary_cloud(new pcl::PointCloud<pcl::PointXYZ>); 
  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);
  pcl::io::loadPCDFile<pcl::PointXYZ>(path_to_point_cloud, *cloud);

  ROS_INFO("LOADED CLOUD");
  I.level_set(level_set_threshold,boundary_cloud);

  ROS_INFO("Calculated level set");

  boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer (new pcl::visualization::PCLVisualizer ("3D Viewer"));
  viewer->addPointCloud (cloud, "cloud"); // Method #1
  viewer->addPointCloud (boundary_cloud, "boundary_cloud"); // Method #1

  // pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGB> rgb(cloud);
  // viewer->addPointCloud<pcl::PointXYZRGB> (cloud, rgb, "cloud"); // Method #1


  viewer->addCoordinateSystem(1.0, 0, 0, 0, 0); 
  viewer->setBackgroundColor (0, 0, 0, 0);   // Setting background to a dark grey 
  viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 1, "cloud"); 
  viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 10, "boundary_cloud"); 
  viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_COLOR, 0.0f, 1.0f, 0.0f, "boundary_cloud");

  std::cout << boundary_cloud->size() << std::endl;
  viewer->initCameraParameters ();

  while (!viewer->wasStopped ()) { 
      viewer->spinOnce (); 
  } 


  return 0;
}