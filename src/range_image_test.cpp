/**
 * @file   range_image_test.cpp
 * @author Sam Zeng
 * @date   11/17/2017
 *
 * @copyright
 * Copyright (C) 2017.
 */

// #include <ros/ros.h>
// #include <sensor_msgs/PointCloud2.h>
// #include <visualization_msgs/Marker.h>

// #include <pcl/point_cloud.h>
// #include <pcl/octree/octree.h>
// #include <pcl/io/pcd_io.h>
// #include <iostream>
// #include <vector>
// #include <ctime>
// #include <pcl/features/normal_3d.h>
// #include <pcl/visualization/cloud_viewer.h>

// #include <boost/thread/thread.hpp>
// #include <pcl/common/common_headers.h>
// #include <pcl/visualization/pcl_visualizer.h>
// #include <Eigen/SVD>
// #include <map>
// #include <fstream>
// #include <math.h> 
// #include <string>

// #include <unistd.h>
// #include <pcl/range_image/range_image.h>
// #include <pcl/visualization/range_image_visualizer.h>


// void estimate_localizablity(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud, pcl::octree::OctreePointCloudSearch<pcl::PointXYZ> octree, pcl::PointCloud<pcl::Normal>::Ptr cloud_normals,  pcl::PointXYZ p, float laser_range){
//   std::cout << "IN estimation" << std::endl;

//   // float ang_res_x = 1 * M_PI/180;
//   // float ang_res_y = 1 * M_PI/180;
//   // Eigen::Vector3f center = Eigen::Vector3f(0,0,0);

//   // pcl::RangeImage rangeImage;

//   // rangeImage.createFromPointCloudWithKnownSize(cloud,ang_res_x,ang_res_y, &center,40);
//   // std::cout << rangeImage << "\n";
//   cloud->width = (uint32_t) cloud->points.size();
//   cloud->height = 1;

//   float angularResolution = (float) (  0.5f * (M_PI/180.0f));  //   1.0 degree in radians
//   float maxAngleWidth     = (float) (360.0f * (M_PI/180.0f));  // 360.0 degree in radians
//   float maxAngleHeight    = (float) (180.0f * (M_PI/180.0f));  // 180.0 degree in radians
//   Eigen::Affine3f sensorPose = (Eigen::Affine3f)Eigen::Translation3f(3.1415f/2, 0.0f, 0.0f);
//   pcl::RangeImage::CoordinateFrame coordinate_frame = pcl::RangeImage::CAMERA_FRAME;
//   float noiseLevel=0.00;
//   float minRange = 0.0f;
//   int borderSize = 1;
  
//   pcl::RangeImage rangeImage;
//   rangeImage.createFromPointCloud(*cloud, angularResolution, maxAngleWidth, maxAngleHeight, sensorPose, coordinate_frame, noiseLevel, minRange, borderSize);
//   std::cout << rangeImage << std::endl;

//   pcl::visualization::RangeImageVisualizer range_image_widget ("Range image");
//   range_image_widget.showRangeImage (rangeImage);
//   while (!range_image_widget.wasStopped ())
//   {
//     range_image_widget.spinOnce ();
//     pcl_sleep (0.01);
//   }
// }



// int main (int argc, char** argv)
// {
//   ros::init(argc, argv, "estimate_localizablity");
//   std::cout << "HELLO WORLD" << std::endl;

//   ros::NodeHandle n;


//   float x_min;
//   float x_max;
//   float dx;

//   float y_min;
//   float y_max;
//   float dy;
 
//   float z_min;
//   float z_max;
//   float dz;
//   float knn_radius;
//   int show_normals;
//   std::string save_path;
//   float color_scale = 5;
//   float threshold = 20;
//   float laser_range = 20;

//   std::string path_to_point_cloud;
//   if(!n.hasParam("x_min")){
//     return 0;
//   }
//   n.getParam("x_min", x_min);
//   n.getParam("x_max", x_max);
//   n.getParam("dx", dx);
//   n.getParam("y_min", y_min);
//   n.getParam("y_max", y_max);
//   n.getParam("dy", dy);
//   n.getParam("z_min", z_min);
//   n.getParam("z_max", z_max);
//   n.getParam("dz", dz);
//   n.getParam("path_to_point_cloud", path_to_point_cloud);
//   n.getParam("knn_radius", knn_radius);
//   n.getParam("show_normals", show_normals);
//   n.getParam("color_scale", color_scale);
//   n.getParam("save_path", save_path);
//   n.getParam("threshold", threshold);
//   n.getParam("laser_range", laser_range);
  
//   pcl::PointCloud<pcl::PointXYZ>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZ>);
//   pcl::PointCloud<pcl::PointXYZRGB>::Ptr localizablity_cloud (new pcl::PointCloud<pcl::PointXYZRGB>);
//   pcl::PointCloud<pcl::Normal>::Ptr localizablity_dirs (new pcl::PointCloud<pcl::Normal>);
//   pcl::PointCloud<pcl::PointXYZ>::Ptr boundary_cloud (new pcl::PointCloud<pcl::PointXYZ>);

//   pcl::io::loadPCDFile<pcl::PointXYZ>(path_to_point_cloud, *cloud);
//   float resolution = 0.15; // .15

//   pcl::octree::OctreePointCloudSearch<pcl::PointXYZ> octree(resolution);
//   octree.setInputCloud(cloud);
//   octree.addPointsFromInputCloud();

//   std::cout<< "BUILT OCTREE" << std::endl;



//   // Create the normal estimation class, and pass the input dataset to it
//   pcl::NormalEstimation<pcl::PointXYZ, pcl::Normal> ne;
//   ne.setInputCloud (cloud);

//   // Create an empty kdtree representation, and pass it to the normal estimation object.
//   // Its content will be filled inside the object, based on the given input dataset (as no other search surface is given).
//   pcl::search::KdTree<pcl::PointXYZ>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZ> ());
//   ne.setSearchMethod (tree);

//   // Output datasets
//   pcl::PointCloud<pcl::Normal>::Ptr cloud_normals (new pcl::PointCloud<pcl::Normal>);
  
//   // Use all neighbors in a sphere of radius
//   ne.setRadiusSearch (knn_radius);
//   // ne.setRadiusSearch (0.5);
//   // Compute the features
//   ne.compute (*cloud_normals);

//   // std::vector<int> indicies;
//   // float nx;
//   // float ny;
//   // float nz;
//   // float K;
//   // pcl::Normal n;
//   // for(int i = 0; i < cloud->size(); ++i){
    
//   //   ne.computePointNormal(*cloud, indicies, nx, ny, nz, K);
//   //   n.normal_x = nx;
//   //   n.normal_y = ny;
//   //   n.normal_z = nz;
//   //   n.x = cloud->points[i].x;
//   //   n.y = cloud->points[i].y;
//   //   n.z = cloud->points[i].z;
//   //   n.curvature = K;
//   //   cloud_normals->push_back(n);

//   // }








//   std::cout<< "BUILT KDTREE" << std::endl;

//   float localizablity = 0;
//   std::vector< int > collision_pts;
//   std::vector< float > collision_distances;
  
//   int length = int(((x_max-x_min)/dx+.9999));
//   int width = int(((y_max-y_min)/dy+.9999));
//   int height = int(((z_max-z_min)/dz+.9999));
//   float L [length][width][height];


//   int i_counter = 0;
//   int j_counter = 0;
//   int k_counter = 0;

//   std::ofstream L_file;
//   L_file.open ("L.txt");
//   L_file << length << std::endl;
//   L_file << width << std::endl;
//   L_file << height << std::endl;
  
//   Eigen::JacobiSVD<Eigen::MatrixXf> svd;
//   for(float x = x_min; x < x_max; x+=dx)
//   {
//     j_counter = 0;
//     for (float y = y_min; y < y_max; y+=dy)
//     {
//       k_counter = 0;
//       for (float z = z_min; z < z_max; z+=dz)
//       {
//         // float z = -2; 
//         // float x = 15;
//         pcl::PointXYZ p = pcl::PointXYZ(x,y,z);
//         std::cout << p << std::endl;
        
//         octree.radiusSearch(p,0.5,collision_pts,collision_distances,0);
//         if(collision_pts.size()>0){
//           std::cout << "Occupied Point" << std::endl;
//           L_file << p.x << " ";
//           L_file << p.y << " ";
//           L_file << p.z << " ";
//           L_file << 0 << std::endl;
//           L[i_counter][j_counter][k_counter] = 0;
//           k_counter++;
//           continue;
//         }
//         pcl::PointXYZRGB p_localizablity;
//         pcl::Normal p_localizablity_dir;
//         // pcl::Normal l_x;
//         // pcl::Normal l_y;
//         // pcl::Normal l_z;



//         estimate_localizablity(cloud,octree,cloud_normals,p,laser_range);

//         // Eigen::MatrixXf::Index minRow, minCol;
//         // float min = svd.singularValues().minCoeff(&minRow, &minCol);

//         // localizablity = svd.singularValues().minCoeff();
//         // p_localizablity_dir.normal_x = (svd.matrixV()(0,minRow));
//         // p_localizablity_dir.normal_y = (svd.matrixV()(1,minRow));
//         // p_localizablity_dir.normal_z = (svd.matrixV()(2,minRow));

//         // p_localizablity.r =  std::min(int(color_scale*(localizablity) + 0.5),255);
//         // // p_localizablity.r = 255;
//         // // //15 for 3
//         // // // 2 for 1
//         // // // 
//         // p_localizablity.g = 0;
//         // p_localizablity.b = std::max(255-int(color_scale*(localizablity) + 0.5), 0);
//         // p_localizablity.x = x;
//         // p_localizablity.y = y;
//         // p_localizablity.z = z;
//         // // l_x.normal_x = p_localizablity_dir.normal_x;
//         // // l_y.normal_y = p_localizablity_dir.normal_y;
//         // // l_z.normal_z = p_localizablity_dir.normal_z;
//         // // p_localizablity_dir.x = x;
//         // // p_localizablity_dir.y = y;
//         // // p_localizablity_dir.z = z;
//         // localizablity_cloud->push_back(p_localizablity);
//         // localizablity_dirs->push_back(p_localizablity_dir);  
//         // // localizablity_cloud->push_back(p_localizablity);  
//         // // localizablity_cloud->push_back(p_localizablity);  
//         // // localizablity_dirs->push_back(l_x);
//         // // localizablity_dirs->push_back(l_y);
//         // // localizablity_dirs->push_back(l_z);
        
        
        
//         // std::cout << localizablity << std::endl;
//         // // std::cout << i_counter << " " << j_counter << " " << k_counter << std::endl;
//         // // std::cout << length << " " << width << " " << height << std::endl;
        
//         // L[i_counter][j_counter][k_counter] = localizablity;
//         // L_file << p.x << " ";
//         // L_file << p.y << " ";
//         // L_file << p.z << " ";
//         // L_file << localizablity << std::endl; //" ";
//         // // L_file << (svd.matrixV()(0,0))*svd.singularValues()[0] + (svd.matrixV()(1,0))*svd.singularValues()[0] + (svd.matrixV()(2,0))*svd.singularValues()[0] << " ";
//         // // L_file << (svd.matrixV()(0,1))*svd.singularValues()[1] + (svd.matrixV()(1,1))*svd.singularValues()[1] + (svd.matrixV()(2,1))*svd.singularValues()[1] << " ";
//         // // L_file << (svd.matrixV()(0,2))*svd.singularValues()[2] + (svd.matrixV()(1,2))*svd.singularValues()[2] + (svd.matrixV()(2,2))*svd.singularValues()[2] << std::endl;
//         k_counter++;
//       }
//       j_counter++;    
//     }
//     i_counter++;
//   }
//   L_file.close();



//   // std::cout << "starting boundary_cloud finding" << std::endl;

//   // for(int i = 0; i <= i_counter; ++i){
//   //   for (int j = 0; j < j_counter; ++j){
//   //     for(int k = 0; k < k_counter; ++k){
//   //       if(L[i][j][k]>threshold){
//   //         for(int i_n=-1; i_n<=1; ++i_n){
//   //           for(int j_n=-1;j_n<=1; ++j_n){
//   //             for(int k_n=-1;k_n<=1;++k_n){
//   //               if(((i+i_n)>=0 && (i+i_n)<i_counter) && ((j+j_n)>=0 && (j+j_n)<j_counter) && ((k+k_n)>=0 && (k+k_n)<k_counter)){
//   //                 // std::cout << i+i_n << " " << j+j_n << " " << k+k_n << std::endl;
//   //                 if(L[i+i_n][j+j_n][k+k_n]<threshold){
//   //                   pcl::PointXYZ p;
//   //                   p.x = x_min+i*dx;
//   //                   p.y = y_min+j*dy;
//   //                   p.z = z_min+k*dz;
//   //                   boundary_cloud->push_back(p);
//   //                 }  
//   //               }
//   //             }
//   //           }
//   //         }
//   //       }
//   //     }
//   //   }
//   // }


//   pcl::PointCloud<pcl::PointXYZI>::Ptr test (new pcl::PointCloud<pcl::PointXYZI>);;

//   for(int i = 0; i<cloud->size(); ++i)
//   {
//     pcl::PointXYZI pn;
//     pn.x = (cloud->points[i]).x;
//     pn.y = (cloud->points[i]).y;
//     pn.z = (cloud->points[i]).z;
//     pn.intensity = (cloud_normals->points[i]).curvature;
//     if(pn.intensity>.06){
//       pn.intensity = 1;
//     }else{
//       pn.intensity = 0;
//     }
//     test->push_back(pn);
//   }



//   if(save_path.compare("") != 0){
//     pcl::io::savePCDFileASCII(save_path, *localizablity_cloud);  
//   }
  
//   boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer (new pcl::visualization::PCLVisualizer ("3D Viewer"));
//   viewer->addPointCloud (cloud, "cloud"); // Method #1
//   // viewer->addPointCloud (boundary_cloud, "boundary_cloud");
//   // viewer->addPointCloud (cloud, "normals"); // Method #1
 
//   pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGB> rgb(localizablity_cloud);

//   viewer->addPointCloud<pcl::PointXYZRGB> (localizablity_cloud, rgb, "localizablity_cloud"); // Method #1
//   viewer->addPointCloudNormals<pcl::PointXYZRGB, pcl::Normal> (localizablity_cloud, localizablity_dirs, 1, 1, "localizablity_dirs");  
//   viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_COLOR, 0.0f, 1.0f, 0.0f, "localizablity_dirs"); 


//   pcl::visualization::PointCloudColorHandlerGenericField<pcl::PointXYZI> handler_k(test,"intensity"); 

//   viewer->addPointCloud<pcl::PointXYZI>(test,handler_k,"K_cloud"); 



//   if(show_normals>0){
//     viewer->addPointCloudNormals<pcl::PointXYZ, pcl::Normal> (cloud, cloud_normals, show_normals, .25, "normals");  
//     viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_COLOR, 1.0f, 0.0f, 0.0f, "normals"); 
//   }
//   // viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_COLOR, 0.0f, 1.0f, 0.0f, "boundary_cloud");
//   // viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_COLOR, 0.0f, 1.0f, 0.0f, "localizablity_cloud"); 
//   viewer->addCoordinateSystem(1.0, 0, 0, 0, 0); 
//   viewer->setBackgroundColor (0, 0, 0, 0);   // Setting background to a dark grey 
//   viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 1, "cloud"); 
//   // viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 10, "boundary_cloud"); 
//   viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 20, "localizablity_cloud"); 

//   viewer->initCameraParameters ();
  

//   while (!viewer->wasStopped ()) { 
//       viewer->spinOnce (); 
//   } 


  

// }


#include <iostream>

#include <boost/thread/thread.hpp>

#include <pcl/common/common_headers.h>
#include <pcl/range_image/range_image.h>
#include <pcl/io/pcd_io.h>
#include <pcl/visualization/range_image_visualizer.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/console/parse.h>

typedef pcl::PointXYZ PointType;

// --------------------
// -----Parameters-----
// --------------------
float angular_resolution_x = 0.5f,
      angular_resolution_y = angular_resolution_x;
pcl::RangeImage::CoordinateFrame coordinate_frame = pcl::RangeImage::CAMERA_FRAME;
bool live_update = false;

// --------------
// -----Help-----
// --------------
void 
printUsage (const char* progName)
{
  std::cout << "\n\nUsage: "<<progName<<" [options] <scene.pcd>\n\n"
            << "Options:\n"
            << "-------------------------------------------\n"
            << "-rx <float>  angular resolution in degrees (default "<<angular_resolution_x<<")\n"
            << "-ry <float>  angular resolution in degrees (default "<<angular_resolution_y<<")\n"
            << "-c <int>     coordinate frame (default "<< (int)coordinate_frame<<")\n"
            << "-l           live update - update the range image according to the selected view in the 3D viewer.\n"
            << "-h           this help\n"
            << "\n\n";
}

void 
setViewerPose (pcl::visualization::PCLVisualizer& viewer, const Eigen::Affine3f& viewer_pose)
{
  Eigen::Vector3f pos_vector = viewer_pose * Eigen::Vector3f(0, 0, 0);
  Eigen::Vector3f look_at_vector = viewer_pose.rotation () * Eigen::Vector3f(0, 0, 1) + pos_vector;
  Eigen::Vector3f up_vector = viewer_pose.rotation () * Eigen::Vector3f(0, -1, 0);
  viewer.setCameraPosition (pos_vector[0], pos_vector[1], pos_vector[2],
                            look_at_vector[0], look_at_vector[1], look_at_vector[2],
                            up_vector[0], up_vector[1], up_vector[2]);
}

// --------------
// -----Main-----
// --------------
int 
main (int argc, char** argv)
{
  // --------------------------------------
  // -----Parse Command Line Arguments-----
  // --------------------------------------
  if (pcl::console::find_argument (argc, argv, "-h") >= 0)
  {
    printUsage (argv[0]);
    return 0;
  }
  if (pcl::console::find_argument (argc, argv, "-l") >= 0)
  {
    live_update = true;
    std::cout << "Live update is on.\n";
  }
  if (pcl::console::parse (argc, argv, "-rx", angular_resolution_x) >= 0)
    std::cout << "Setting angular resolution in x-direction to "<<angular_resolution_x<<"deg.\n";
  if (pcl::console::parse (argc, argv, "-ry", angular_resolution_y) >= 0)
    std::cout << "Setting angular resolution in y-direction to "<<angular_resolution_y<<"deg.\n";
  int tmp_coordinate_frame;
  if (pcl::console::parse (argc, argv, "-c", tmp_coordinate_frame) >= 0)
  {
    coordinate_frame = pcl::RangeImage::CoordinateFrame (tmp_coordinate_frame);
    std::cout << "Using coordinate frame "<< (int)coordinate_frame<<".\n";
  }
  angular_resolution_x = pcl::deg2rad (angular_resolution_x);
  angular_resolution_y = pcl::deg2rad (angular_resolution_y);
  
  // ------------------------------------------------------------------
  // -----Read pcd file or create example point cloud if not given-----
  // ------------------------------------------------------------------
  pcl::PointCloud<PointType>::Ptr point_cloud_ptr (new pcl::PointCloud<PointType>);
  pcl::PointCloud<PointType>& point_cloud = *point_cloud_ptr;
  Eigen::Affine3f scene_sensor_pose (Eigen::Affine3f::Identity ());
  std::vector<int> pcd_filename_indices = pcl::console::parse_file_extension_argument (argc, argv, "pcd");
  if (!pcd_filename_indices.empty ())
  {
    std::string filename = argv[pcd_filename_indices[0]];
    if (pcl::io::loadPCDFile (filename, point_cloud) == -1)
    {
      std::cout << "Was not able to open file \""<<filename<<"\".\n";
      printUsage (argv[0]);
      return 0;
    }
    scene_sensor_pose = Eigen::Affine3f (Eigen::Translation3f (point_cloud.sensor_origin_[0],
                                                             point_cloud.sensor_origin_[1],
                                                             point_cloud.sensor_origin_[2])) *
                        Eigen::Affine3f (point_cloud.sensor_orientation_);
  }
  else
  {
    std::cout << "\nNo *.pcd file given => Genarating example point cloud.\n\n";
    for (float x=-0.5f; x<=0.5f; x+=0.01f)
    {
      for (float y=-0.5f; y<=0.5f; y+=0.01f)
      {
        PointType point;  point.x = x;  point.y = y;  point.z = 2.0f - y;
        point_cloud.points.push_back (point);
      }
    }
    point_cloud.width = (int) point_cloud.points.size ();  point_cloud.height = 1;
  }
  
  // -----------------------------------------------
  // -----Create RangeImage from the PointCloud-----
  // -----------------------------------------------
  float noise_level = 0.0;
  float min_range = 0.0f;
  int border_size = 1;
  boost::shared_ptr<pcl::RangeImage> range_image_ptr(new pcl::RangeImage);
  pcl::RangeImage& range_image = *range_image_ptr;   
  range_image.createFromPointCloud (point_cloud, angular_resolution_x, angular_resolution_y,
                                    pcl::deg2rad (360.0f), pcl::deg2rad (180.0f),
                                    scene_sensor_pose, coordinate_frame, noise_level, min_range, border_size);
  
  // --------------------------------------------
  // -----Open 3D viewer and add point cloud-----
  // --------------------------------------------
  pcl::visualization::PCLVisualizer viewer ("3D Viewer");
  viewer.setBackgroundColor (1, 1, 1);
  pcl::visualization::PointCloudColorHandlerCustom<pcl::PointWithRange> range_image_color_handler (range_image_ptr, 0, 0, 0);
  viewer.addPointCloud (range_image_ptr, range_image_color_handler, "range image");
  viewer.setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 1, "range image");
  //viewer.addCoordinateSystem (1.0f, "global");
  //PointCloudColorHandlerCustom<PointType> point_cloud_color_handler (point_cloud_ptr, 150, 150, 150);
  //viewer.addPointCloud (point_cloud_ptr, point_cloud_color_handler, "original point cloud");
  viewer.initCameraParameters ();
  setViewerPose(viewer, range_image.getTransformationToWorldSystem ());
  
  // --------------------------
  // -----Show range image-----
  // --------------------------
  pcl::visualization::RangeImageVisualizer range_image_widget ("Range image");
  range_image_widget.showRangeImage (range_image);
  
  //--------------------
  // -----Main loop-----
  //--------------------
  while (!viewer.wasStopped ())
  {
    range_image_widget.spinOnce ();
    viewer.spinOnce ();
    pcl_sleep (0.01);
    
    if (live_update)
    {
      scene_sensor_pose = viewer.getViewerPose();
      range_image.createFromPointCloud (point_cloud, angular_resolution_x, angular_resolution_y,
                                        pcl::deg2rad (360.0f), pcl::deg2rad (180.0f),
                                        scene_sensor_pose, pcl::RangeImage::LASER_FRAME, noise_level, min_range, border_size);
      range_image_widget.showRangeImage (range_image);
    }
  }
}