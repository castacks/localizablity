/**
 * @file   view_map.cpp
 * @author Sam Zeng
 * @date   11/17/2017
 *
 * @copyright
 * Copyright (C) 2017.
 */

#include <localizablity/view_map.h>

view_map::view_map(float res){
	count = 0;
	resolution = res;
	length = int(360/resolution) * int(180/resolution);
	normal_data = Eigen::MatrixX3f(length,3);
	curvature_data = Eigen::MatrixXf(length,3);
	density_mask = Eigen::MatrixXf(length,3);

}


Eigen::Vector3f view_map::sphere_angles_to_vector(float phi, float theta, float yaw){
	float z = sin(theta) * cos(phi);
	float y = sin(theta) * sin(phi);
	float x = cos(theta);
	float x_new = x*cos(yaw) - y*sin(yaw);
	float y_new = x*sin(yaw) + y*cos(yaw);
	return Eigen::Vector3f(x_new,y_new,z);
}

void view_map::add_point(double phi, double theta, pcl::PointXYZ p, pcl::Normal n, double density_angle){
	if(count>=length){
		count ++;
		ROS_ERROR("MORE POINTS THAN EXPECTED %d", count - length);
		return;
	}
	normal_data(count,0) = n.normal_x;
	normal_data(count,1) = n.normal_y;
	normal_data(count,2) = n.normal_z;
	curvature_data(count,0) = n.curvature;
	curvature_data(count,1) = n.curvature;
	curvature_data(count,2) = n.curvature;
	density_mask(count,0) = 1 + fabs(cos(density_angle));
	density_mask(count,1) = 1 + fabs(cos(density_angle));
	density_mask(count,2) = 1 + fabs(cos(density_angle));
	count++;
}	