/**
 * @file   localizability_volume.cpp
 * @author Sam Zeng
 * @date   11/17/2017
 *
 * @copyright
 * Copyright (C) 2017.
 */

#include <localizablity/Calculate_Localizablity.h>
#include <ros/package.h>

int main(int argc, char** argv)
{
  ros::init(argc, argv, "localizablity_volume");
  ros::NodeHandle n;
  float x_min;
  float x_max;
  float dx;

  float y_min;
  float y_max;
  float dy;
   
  float z_min;
  float z_max;
  float dz;

  float theta = M_PI/2;

  n.getParam("x_min", x_min);
  n.getParam("x_max", x_max);
  n.getParam("dx", dx);
  n.getParam("y_min", y_min);
  n.getParam("y_max", y_max);
  n.getParam("dy", dy);
  n.getParam("z_min", z_min);
  n.getParam("z_max", z_max);
  n.getParam("dz", dz);
  n.getParam("theta",theta);

  Calculate_Localizablity c = Calculate_Localizablity(n);
  std::ofstream L_file;
  std::string path = ros::package::getPath("localizablity");
  L_file.open((path+"/data/L.txt").c_str());

  int length = int(((x_max-x_min)/dx+.9999));
  int width = int(((y_max-y_min)/dy+.9999));
  int height = int(((z_max-z_min)/dz+.9999));
  L_file << length << std::endl;
  L_file << width << std::endl;
  L_file << height << std::endl;


  for(float x = x_min; x < x_max; x+=dx)
  {
    for (float y = y_min; y < y_max; y+=dy)
    {
      for (float z = z_min; z < z_max; z+=dz)
      {
      float L = c.estimate_localizablity(x,y,z,theta);
      L_file << x << " ";
      L_file << y << " ";
      L_file << z << " ";
      L_file << L << std::endl; //" ";

      // std::cout << "LOCALIZABLITY: " << L << std::endl;
      }
    }
  }
  L_file.close();
  c.visualize();
  
  return 0;
}

