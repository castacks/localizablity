/**
 * @file   test.cpp
 * @author Sam Zeng
 * @date   11/17/2017
 *
 * @copyright
 * Copyright (C) 2017.
 */

#include <pcl/point_cloud.h>
#include <pcl/octree/octree.h>
#include <pcl/io/pcd_io.h>
#include <iostream>
#include <vector>
#include <ctime>
#include <pcl/features/normal_3d.h>
#include <pcl/visualization/cloud_viewer.h>

#include <boost/thread/thread.hpp>
#include <pcl/common/common_headers.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <Eigen/SVD>
#include <map>
#include <fstream>
#include <math.h> 


bool float_cmp(float f1, float f2, float tolerance){
  if( ((f1-f2)*(f1-f2)) < tolerance*tolerance){
    return true;
  }else{
    return false;
  }
}


bool point_cmp(pcl::PointXYZ p1, pcl::PointXYZ p2){
  float x_mult = 10000000000;
  float y_mult = 1000000;
  float z_mult = 100;
  return (p1.x*x_mult + p1.y*y_mult + p1.z*z_mult) > (p2.x*x_mult + p2.y*y_mult + p2.z*z_mult);
}



float estimate_localizablity(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud, pcl::octree::OctreePointCloudSearch<pcl::PointXYZ> octree, pcl::PointCloud<pcl::Normal>::Ptr cloud_normals,  pcl::PointXYZ p, pcl::PointXYZRGB* localizablity_p){
  float resolution = 0.15; // .15

  pcl::PointCloud<pcl::PointXYZ>::Ptr actual_visiable (new pcl::PointCloud<pcl::PointXYZ>);
  pcl::PointCloud<pcl::PointXYZ>::Ptr viewpoint (new pcl::PointCloud<pcl::PointXYZ>);
  pcl::PointCloud<pcl::Normal>::Ptr normals_in_range (new pcl::PointCloud<pcl::Normal>);
  pcl::PointCloud<pcl::Normal>::Ptr visible_normals_pc (new pcl::PointCloud<pcl::Normal>);

  pcl::PointCloud<pcl::PointXYZ>::Ptr visible_cloud (new pcl::PointCloud<pcl::PointXYZ>);
  std::vector< int > visible_indicies;
  std::vector< float > visible_distances;
  Eigen::Vector3f origin;
  Eigen::Vector3f direction;

  viewpoint->push_back(p);
  origin[0] = p.x;
  origin[1] = p.y;
  origin[2] = p.z;

  octree.radiusSearch(p,15,visible_indicies,visible_distances,0);

  for(int i;i<visible_indicies.size();++i){
    visible_cloud->push_back(cloud->points[visible_indicies[i]]);
    normals_in_range->push_back(cloud_normals->points[visible_indicies[i]]);
  }

  pcl::octree::OctreePointCloudSearch<pcl::PointXYZ> octree_visible(resolution);
  octree_visible.setInputCloud(visible_cloud);
  octree_visible.addPointsFromInputCloud();


  Eigen::MatrixX3f visible_normals(visible_cloud->size()*3,3);
  
  std::vector<pcl::PointXYZ, Eigen::aligned_allocator<pcl::PointXYZ> > voxel_centroids;
  
  
  // bool(*fn_pt)(pcl::PointXYZ,pcl::PointXYZ) = point_cmp;
  // std::map<pcl::PointXYZ,int,bool(*)(pcl::PointXYZ,pcl::PointXYZ)> checked_voxels (fn_pt);
  // octree_centroids.getVoxelCentroids(node_centroid);
  // octree.getVoxelCentroids(cloud_centroids);

  // pcl::octree_iterator(&octree);
  octree_visible.getOccupiedVoxelCenters(voxel_centroids);
  int counter = 0;
  for(int i=0;i<voxel_centroids.size();++i){
    std::vector<pcl::PointXYZ, Eigen::aligned_allocator<pcl::PointXYZ> > visible_centroids;


    direction[0] = voxel_centroids[i].x - origin[0];
    direction[1] = voxel_centroids[i].y - origin[1];
    direction[2] = voxel_centroids[i].z - origin[2];
    std::vector< int > intersectedVoxels;

    int count = octree_visible.getIntersectedVoxelIndices(origin,direction,intersectedVoxels,1);
    // octree_visible.getIntersectedVoxelCenters(origin,direction,visible_centroids,1);
    // std::cout << visible_centroids[0] << " " << visible_centroids.size() << " " << checked_voxels.size() << " " << checked_voxels.count(visible_centroids[0]) << std::endl;
    // if(checked_voxels.count(visible_centroids[0])){
    //   continue;
    // }else{
    //   checked_voxels[visible_centroids[0]] = 1;
    // }
    // if(intersectedVoxels.size() == 1){
    // std::cout << i << std::endl;
    if(count > 0){
      float angle;
      for(int j=0;j<intersectedVoxels.size();j+=1){
        if(isnan((normals_in_range->points[intersectedVoxels[j]]).normal_x)){
          continue;
        }

        Eigen::Vector3f n = Eigen::Vector3f(normals_in_range->points[intersectedVoxels[j]].normal_x,normals_in_range->points[intersectedVoxels[j]].normal_y,normals_in_range->points[intersectedVoxels[j]].normal_z);
        angle = atan2( direction.cross(n).norm(), direction.dot(n));
        if((abs(angle) > 1.22173 && abs(angle) < 1.91986) ){ // ~70 degrees && ~110 degrees
          continue;
        }

        actual_visiable->push_back(visible_cloud->points[intersectedVoxels[j]]);
        visible_normals(counter,0) = (normals_in_range->points[intersectedVoxels[j]]).normal_x;
        visible_normals(counter,1) = (normals_in_range->points[intersectedVoxels[j]]).normal_y;
        visible_normals(counter,2) = (normals_in_range->points[intersectedVoxels[j]]).normal_z;
        visible_normals_pc->push_back(normals_in_range->points[intersectedVoxels[j]]);
        counter++;
        if(counter>=visible_normals.rows()){
          visible_normals.resize(visible_normals.rows()*2,3);
        }
      }
        
    }
    // }

  }

  // boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer (new pcl::visualization::PCLVisualizer ("3D Viewer"));
  // viewer->addPointCloud (cloud, "cloud"); // Method #1
  // // viewer->addPointCloud (actual_visiable, "normals"); // Method #1
  // viewer->addPointCloud (viewpoint, "viewpoint"); // Method #1
  
  // std::cout << actual_visiable->size() << " " << cloud->size() << std::endl; 
  // viewer->addPointCloudNormals<pcl::PointXYZ, pcl::Normal> (cloud, cloud_normals, 1, .25, "normals");
  // viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_COLOR, 1.0f, 0.0f, 0.0f, "normals"); 
  // viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_COLOR, 0.0f, 1.0f, 0.0f, "viewpoint"); 

  // viewer->addCoordinateSystem(1.0, 0, 0, 0, 0); 
  // viewer->setBackgroundColor (0.05, 0.05, 0.05, 0);   // Setting background to a dark grey 
  // viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 1, "cloud"); 
  // viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 3, "normals"); 
  // viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 5, "viewpoint"); 

  // viewer->initCameraParameters ();
  

  // while (!viewer->wasStopped ()) { 
  //     viewer->spinOnce (); 
  // } 

  if(counter == 0){
    return 0;
  }

 visible_normals.conservativeResize(counter,3);


  Eigen::JacobiSVD<Eigen::MatrixXf> svd(visible_normals, Eigen::ComputeThinU | Eigen::ComputeThinV);
  // std::cout << "Its singular values are:" << std::endl << svd.singularValues()[0] << std::endl;
  // cout << "Its left singular vectors are the columns of the thin U matrix:" << endl << svd.matrixU() << endl;
  // std::cout << "Its singular values are:" << std::endl << svd.singularValues()[1] << std::endl;
  // std::cout << "Its singular values are:" << std::endl << svd.singularValues()[2] << std::endl;
  
  // localizablity_p->r = std::min(int((svd.singularValues()[0])*3 + 0.5),255);
  // localizablity_p->g = std::min(int((svd.singularValues()[1])*3 + 0.5),255);
  // localizablity_p->b = std::min(int((svd.singularValues()[2])*3 + 0.5),255);
  
  return svd.singularValues().minCoeff();
}






int main (int argc, char** argv)
{
  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZ>);
  pcl::PointCloud<pcl::PointXYZRGB>::Ptr localizablity_cloud (new pcl::PointCloud<pcl::PointXYZRGB>);

  pcl::io::loadPCDFile<pcl::PointXYZ>("/home/slz/Desktop/big_run2.pcd", *cloud);
  float resolution = 0.15; // .15

  pcl::octree::OctreePointCloudSearch<pcl::PointXYZ> octree(resolution);
  octree.setInputCloud(cloud);
  octree.addPointsFromInputCloud();

  std::cout<< "BUILT OCTREE" << std::endl;



  // Create the normal estimation class, and pass the input dataset to it
  pcl::NormalEstimation<pcl::PointXYZ, pcl::Normal> ne;
  ne.setInputCloud (cloud);

  // Create an empty kdtree representation, and pass it to the normal estimation object.
  // Its content will be filled inside the object, based on the given input dataset (as no other search surface is given).
  pcl::search::KdTree<pcl::PointXYZ>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZ> ());
  ne.setSearchMethod (tree);

  // Output datasets
  pcl::PointCloud<pcl::Normal>::Ptr cloud_normals (new pcl::PointCloud<pcl::Normal>);
  
  // Use all neighbors in a sphere of radius
  // ne.setRadiusSearch (1.0);
  ne.setRadiusSearch (0.5);
  // Compute the features
  ne.compute (*cloud_normals);
  std::cout<< "BUILT KDTREE" << std::endl;

  float localizablity = 0;
  std::vector< int > collision_pts;
  std::vector< float > collision_distances;
  
  // for(float x = 0; x<30; x+=1)
  // {
    for (float y = -40; y < -5; y+=1)
    {
      for (float z = -5; z < 10; z+=0.5)
          {
            // float z = -2; 
            float x = 15;
            pcl::PointXYZ p = pcl::PointXYZ(x,y,z);
            std::cout << p << std::endl;
            
            octree.radiusSearch(p,0.75,collision_pts,collision_distances,0);
            if(collision_pts.size()>0){
              std::cout << "Occupied Point" << std::endl;
              continue;
            }
            pcl::PointXYZRGB p_localizablity;

            localizablity = estimate_localizablity(cloud,octree,cloud_normals,p,&p_localizablity);
            p_localizablity.r =  std::min(int(5*(localizablity) + 0.5),255);
            // // p_localizablity.r = 255;
            // //15 for 3
            // // 2 for 1
            // // 
            p_localizablity.g = 0;
            p_localizablity.b = std::max(255-int(5*(localizablity) + 0.5), 0);
            p_localizablity.x = x;
            p_localizablity.y = y;
            p_localizablity.z = z;
            localizablity_cloud->push_back(p_localizablity);   
            std::cout << localizablity << std::endl;


          }    
    }
  // }


  boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer (new pcl::visualization::PCLVisualizer ("3D Viewer"));
  viewer->addPointCloud (cloud, "cloud"); // Method #1
  // viewer->addPointCloud (actual_visiable, "normals"); // Method #1
  pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGB> rgb(localizablity_cloud);

  viewer->addPointCloud<pcl::PointXYZRGB> (localizablity_cloud, rgb, "localizablity_cloud"); // Method #1
  // viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_COLOR, 0.0f, 1.0f, 0.0f, "localizablity_cloud"); 
  viewer->addCoordinateSystem(1.0, 0, 0, 0, 0); 
  viewer->setBackgroundColor (0, 0, 0, 0);   // Setting background to a dark grey 
  viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 1, "cloud"); 
  viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 10, "localizablity_cloud"); 

  viewer->initCameraParameters ();
  

  while (!viewer->wasStopped ()) { 
      viewer->spinOnce (); 
  } 


  
  pcl::io::savePCDFileASCII("localizablity.pcd", *localizablity_cloud);




}


