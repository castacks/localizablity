/**
 * @file   real_time_test.cpp
 * @author Sam Zeng
 * @date   11/17/2017
 *
 * @copyright
 * Copyright (C) 2017.
 */

#include <ros/ros.h>
#include <sensor_msgs/PointCloud2.h>
#include <visualization_msgs/Marker.h>

#include <pcl/point_cloud.h>
#include <pcl/octree/octree.h>
#include <pcl/io/pcd_io.h>
#include <iostream>
#include <vector>
#include <ctime>
#include <pcl/features/normal_3d.h>
#include <pcl/visualization/cloud_viewer.h>

#include <boost/thread/thread.hpp>
#include <pcl/common/common_headers.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <Eigen/SVD>
#include <map>
#include <fstream>
#include <math.h> 
#include <string>

#include <unistd.h>
#include <tf/transform_listener.h>

#include <geometry_msgs/Point.h>
#include <std_msgs/Float64.h>

float estimate_localizablity(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud, pcl::octree::OctreePointCloudSearch<pcl::PointXYZ> octree, pcl::PointCloud<pcl::Normal>::Ptr cloud_normals,  pcl::PointXYZ p, pcl::Normal* localizablity_p, float laser_range){
  float resolution = 0.15; // .15

  pcl::PointCloud<pcl::PointXYZ>::Ptr actual_visiable (new pcl::PointCloud<pcl::PointXYZ>);
  pcl::PointCloud<pcl::PointXYZ>::Ptr viewpoint (new pcl::PointCloud<pcl::PointXYZ>);
  pcl::PointCloud<pcl::Normal>::Ptr normals_in_range (new pcl::PointCloud<pcl::Normal>);
  pcl::PointCloud<pcl::Normal>::Ptr visible_normals_pc (new pcl::PointCloud<pcl::Normal>);

  pcl::PointCloud<pcl::PointXYZ>::Ptr visible_cloud (new pcl::PointCloud<pcl::PointXYZ>);
  std::vector< int > visible_indicies;
  std::vector< float > visible_distances;
  Eigen::Vector3f origin;
  Eigen::Vector3f direction;

  viewpoint->push_back(p);
  origin[0] = p.x;
  origin[1] = p.y;
  origin[2] = p.z;

  octree.radiusSearch(p,laser_range,visible_indicies,visible_distances,0);

  for(int i = 0;i<visible_indicies.size();++i){
    visible_cloud->push_back(cloud->points[visible_indicies[i]]);
    normals_in_range->push_back(cloud_normals->points[visible_indicies[i]]);
  }

  pcl::octree::OctreePointCloudSearch<pcl::PointXYZ> octree_visible(resolution);
  octree_visible.setInputCloud(visible_cloud);
  octree_visible.addPointsFromInputCloud();


  Eigen::MatrixX3f visible_normals(visible_cloud->size()*3,3);
  
  std::vector<pcl::PointXYZ, Eigen::aligned_allocator<pcl::PointXYZ> > voxel_centroids;
  
  
  // bool(*fn_pt)(pcl::PointXYZ,pcl::PointXYZ) = point_cmp;
  // std::map<pcl::PointXYZ,int,bool(*)(pcl::PointXYZ,pcl::PointXYZ)> checked_voxels (fn_pt);
  // octree_centroids.getVoxelCentroids(node_centroid);
  // octree.getVoxelCentroids(cloud_centroids);

  // pcl::octree_iterator(&octree);
  octree_visible.getOccupiedVoxelCenters(voxel_centroids);
  int counter = 0;
  std::vector< int > intersectedVoxels;
  float angle;
  int count;

  for(int i=0;i<voxel_centroids.size();++i){
    std::vector<pcl::PointXYZ, Eigen::aligned_allocator<pcl::PointXYZ> > visible_centroids;


    direction[0] = voxel_centroids[i].x - origin[0];
    direction[1] = voxel_centroids[i].y - origin[1];
    direction[2] = voxel_centroids[i].z - origin[2];

    count = octree_visible.getIntersectedVoxelIndices(origin,direction,intersectedVoxels,1);
    // octree_visible.getIntersectedVoxelCenters(origin,direction,visible_centroids,1);
    // std::cout << visible_centroids[0] << " " << visible_centroids.size() << " " << checked_voxels.size() << " " << checked_voxels.count(visible_centroids[0]) << std::endl;
    // if(checked_voxels.count(visible_centroids[0])){
    //   continue;
    // }else{
    //   checked_voxels[visible_centroids[0]] = 1;
    // }
    // if(intersectedVoxels.size() == 1){
    // std::cout << intersectedVoxels.size() << std::endl;
    if(count > 0){
      
      float distance = sqrt( pow(voxel_centroids[i].x-origin[0],2) + pow(voxel_centroids[i].y-origin[1],2) + pow(voxel_centroids[i].z-origin[2],2)  );
      float unit_sphere_angle = asin((resolution/2)/distance);
      float unit_area = 2*M_PI*(1-cos(unit_sphere_angle));

      for(int j=0;j<intersectedVoxels.size();j+=1){
        if(isnan((normals_in_range->points[intersectedVoxels[j]]).normal_x)){
          // std::cout << "NAN normal value" << std::endl;
          continue;
        }

        Eigen::Vector3f n = Eigen::Vector3f(normals_in_range->points[intersectedVoxels[j]].normal_x,normals_in_range->points[intersectedVoxels[j]].normal_y,normals_in_range->points[intersectedVoxels[j]].normal_z);
        angle = atan2( direction.cross(n).norm(), direction.dot(n));
        if((std::fabs(angle) > 1.22173 && std::fabs(angle) < 1.91986) ){ // ~70 degrees (1.22173 rad) && ~110 (1.91986) degrees
          // std::cout << "Angle too big" << std::endl;
          continue;
        }

        actual_visiable->push_back(visible_cloud->points[intersectedVoxels[j]]);
        visible_normals(counter,0) = (normals_in_range->points[intersectedVoxels[j]]).normal_x * unit_area/(intersectedVoxels.size());
        visible_normals(counter,1) = (normals_in_range->points[intersectedVoxels[j]]).normal_y * unit_area/(intersectedVoxels.size());
        visible_normals(counter,2) = (normals_in_range->points[intersectedVoxels[j]]).normal_z * unit_area/(intersectedVoxels.size());
        visible_normals_pc->push_back(normals_in_range->points[intersectedVoxels[j]]);
        counter++;
        if(counter>=visible_normals.rows()){
          visible_normals.resize(visible_normals.rows()*2,3);
        }
      }
        
    }else{
      std::cout << "No intersectedVoxels" << std::endl;
    }
    // }

  }

  // unsigned int usecs = 1000000;
  // usleep(usecs);


  // boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer (new pcl::visualization::PCLVisualizer ("3D Viewer"));
  // viewer->addPointCloud (cloud, "full_cloud"); // Method #1
  // viewer->addPointCloud (visible_cloud, "cloud"); // Method #1
  // // viewer->addPointCloud (actual_visiable, "normals"); // Method #1
  // viewer->addPointCloud (viewpoint, "viewpoint"); // Method #1
  
  // std::cout << actual_visiable->size() << " " << cloud->size() << std::endl; 
  // viewer->addPointCloudNormals<pcl::PointXYZ, pcl::Normal> (visible_cloud, normals_in_range, 1, .25, "normals");
  // viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_COLOR, 1.0f, 0.0f, 0.0f, "normals"); 
  // viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_COLOR, 0.0f, 1.0f, 0.0f, "viewpoint"); 
  // viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_COLOR, 0.0f, 0.0f, 1.0f, "full_cloud"); 

  // viewer->addCoordinateSystem(1.0, 0, 0, 0, 0); 
  // viewer->setBackgroundColor (0.05, 0.05, 0.05, 0);   // Setting background to a dark grey 
  // viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 1, "full_cloud"); 
  // viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 2, "cloud"); 
  // viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 3, "normals"); 
  // viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 5, "viewpoint"); 

  // viewer->initCameraParameters ();
  

  // while (!viewer->wasStopped ()) { 
  //     viewer->spinOnce (); 
  // } 

  if(counter == 0){
    std::cout << "NO VALID POINTS" << std::endl;
    return 0;
  }

 visible_normals.conservativeResize(counter,3);


  Eigen::JacobiSVD<Eigen::MatrixXf> svd(visible_normals, Eigen::ComputeThinU | Eigen::ComputeThinV);
  // std::cout << "Its left singular vectors are the columns of the thin V matrix:" << std::endl << svd.matrixV() << std::endl;
  // std::cout << "Its singular values are:" << std::endl << svd.singularValues() << std::endl;
  
  // localizablity_p->r = std::min(int((svd.singularValues()[0])*3 + 0.5),255);
  // localizablity_p->g = std::min(int((svd.singularValues()[1])*3 + 0.5),255);
  // localizablity_p->b = std::min(int((svd.singularValues()[2])*3 + 0.5),255);
  // localizablity_p->normal_x = std::min((svd.singularValues()[0])/50,10000.0f);
  // localizablity_p->normal_y = std::min((svd.singularValues()[1])/50,10000.0f);
  // localizablity_p->normal_z = std::min((svd.singularValues()[2])/50,10000.0f);
  

  Eigen::MatrixXf::Index minRow, minCol;
  float min = svd.singularValues().minCoeff(&minRow, &minCol);

  localizablity_p->normal_x = (svd.matrixV()(0,minRow));
  localizablity_p->normal_y = (svd.matrixV()(1,minRow));
  localizablity_p->normal_z = (svd.matrixV()(2,minRow));
  // std::cout << minRow << " " << minCol << std::endl;
  std::cout << localizablity_p->normal_x << " " << localizablity_p-> normal_y << " " << localizablity_p->normal_z << std::endl;
  return svd.singularValues().minCoeff();
}


// void get_neighbor_indicies(int* indicies[], int length, int width, int height, bool use_26){
//   if(!use_26){

//   }else{
//     for(int i=-1; i<=1; ++i){
//       for(int j=-1;j<=1; ++j){
//         for(int k=-1;k<=1;++k){
//           indicies[count] = {i,j,k}
//         }
//       }
//     }
//   }
// }


int main (int argc, char** argv)
{
  ros::init(argc, argv, "estimate_localizablity");

  ros::NodeHandle n;

  tf::TransformListener listener;
  ros::Publisher vis_pub = n.advertise<visualization_msgs::Marker>( "visualization_marker", 0 );
  ros::Publisher localizablity_pub = n.advertise<std_msgs::Float64>( "localizablity", 0 );
  ros::Publisher inverse_localizablity_pub = n.advertise<std_msgs::Float64>( "inverse_localizablity", 0 );
  ros::Publisher state_estimation_error_pub = n.advertise<std_msgs::Float64>( "state_estimation_error", 0 );


  float x_min;
  float x_max;
  float dx;

  float y_min;
  float y_max;
  float dy;
 
  float z_min;
  float z_max;
  float dz;
  float knn_radius;
  int show_normals;
  std::string save_path;
  float color_scale = 5;
  float threshold = 20;
  float laser_range = 20;

  std::string path_to_point_cloud;
  if(!n.hasParam("x_min")){
    return 0;
  }
  n.getParam("x_min", x_min);
  n.getParam("x_max", x_max);
  n.getParam("dx", dx);
  n.getParam("y_min", y_min);
  n.getParam("y_max", y_max);
  n.getParam("dy", dy);
  n.getParam("z_min", z_min);
  n.getParam("z_max", z_max);
  n.getParam("dz", dz);
  n.getParam("path_to_point_cloud", path_to_point_cloud);
  n.getParam("knn_radius", knn_radius);
  n.getParam("show_normals", show_normals);
  n.getParam("color_scale", color_scale);
  n.getParam("save_path", save_path);
  n.getParam("threshold", threshold);
  n.getParam("laser_range", laser_range);
  
  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZ>);
  pcl::PointCloud<pcl::PointXYZRGB>::Ptr localizablity_cloud (new pcl::PointCloud<pcl::PointXYZRGB>);
  pcl::PointCloud<pcl::Normal>::Ptr localizablity_dirs (new pcl::PointCloud<pcl::Normal>);
  pcl::PointCloud<pcl::PointXYZ>::Ptr boundary_cloud (new pcl::PointCloud<pcl::PointXYZ>);

  pcl::io::loadPCDFile<pcl::PointXYZ>(path_to_point_cloud, *cloud);
  float resolution = 0.2; // .15

  pcl::octree::OctreePointCloudSearch<pcl::PointXYZ> octree(resolution);
  octree.setInputCloud(cloud);
  octree.addPointsFromInputCloud();

  std::cout<< "BUILT OCTREE" << std::endl;



  // Create the normal estimation class, and pass the input dataset to it
  pcl::NormalEstimation<pcl::PointXYZ, pcl::Normal> ne;
  ne.setInputCloud (cloud);

  // Create an empty kdtree representation, and pass it to the normal estimation object.
  // Its content will be filled inside the object, based on the given input dataset (as no other search surface is given).
  pcl::search::KdTree<pcl::PointXYZ>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZ> ());
  ne.setSearchMethod (tree);

  // Output datasets
  pcl::PointCloud<pcl::Normal>::Ptr cloud_normals (new pcl::PointCloud<pcl::Normal>);
  
  // Use all neighbors in a sphere of radius
  ne.setRadiusSearch (knn_radius);
  // ne.setRadiusSearch (0.5);
  // Compute the features
  ne.compute (*cloud_normals);
  std::cout<< "BUILT KDTREE" << std::endl;

  float localizablity = 0;
  std::vector< int > collision_pts;
  std::vector< float > collision_distances;
  

  while(ros::ok()){
    pcl::PointXYZ p;
    tf::StampedTransform transform;
    try{
      listener.lookupTransform("/gazebo_world", "/dji_link",  
                               ros::Time(0), transform);
    }
    catch (tf::TransformException ex){
      ROS_ERROR("%s",ex.what());
      ros::Duration(1.0).sleep();
    }

    p.x = transform.getOrigin().x();
    p.y = transform.getOrigin().y();
    p.z = transform.getOrigin().z();

    try{
      listener.lookupTransform("/base_link", "/body",  
                               ros::Time(0), transform);
    }
    catch (tf::TransformException ex){
      ROS_ERROR("%s",ex.what());
      ros::Duration(1.0).sleep();
    }

    float state_estimation_error = sqrt(transform.getOrigin().x()*transform.getOrigin().x() + transform.getOrigin().y()*transform.getOrigin().y() + transform.getOrigin().z()*transform.getOrigin().z());

    std::cout << p << std::endl;
    
    // octree.radiusSearch(p,0.75,collision_pts,collision_distances,0);
    // if(collision_pts.size()>0){
    //   std::cout << "Occupied Point" << std::endl;
    //   continue;
    // }
    pcl::PointXYZRGB p_localizablity;
    pcl::Normal p_localizablity_dir;
    // pcl::Normal l_x;
    // pcl::Normal l_y;
    // pcl::Normal l_z;

    localizablity = estimate_localizablity(cloud,octree,cloud_normals,p,&p_localizablity_dir,laser_range);

    p_localizablity.r =  std::min(int(color_scale*(localizablity) + 0.5),255);
    // p_localizablity.r = 255;
    // //15 for 3
    // // 2 for 1
    // // 
    p_localizablity.g = 0;
    p_localizablity.b = std::max(255-int(color_scale*(localizablity) + 0.5), 0);
    p_localizablity.x = p.x;
    p_localizablity.y = p.y;
    p_localizablity.z = p.z;
    // l_x.normal_x = p_localizablity_dir.normal_x;
    // l_y.normal_y = p_localizablity_dir.normal_y;
    // l_z.normal_z = p_localizablity_dir.normal_z;
    // p_localizablity_dir.x = x;
    // p_localizablity_dir.y = y;
    // p_localizablity_dir.z = z;
    localizablity_cloud->push_back(p_localizablity);
    localizablity_dirs->push_back(p_localizablity_dir);  
    // localizablity_cloud->push_back(p_localizablity);  
    // localizablity_cloud->push_back(p_localizablity);  
    // localizablity_dirs->push_back(l_x);
    // localizablity_dirs->push_back(l_y);
    // localizablity_dirs->push_back(l_z);
    
    visualization_msgs::Marker marker;
    marker.header.frame_id = "/gazebo_world";
    marker.header.stamp = ros::Time();
    marker.ns = "my_namespace";
    marker.id = 0;
    marker.type = visualization_msgs::Marker::LINE_STRIP;
    marker.action = visualization_msgs::Marker::ADD;
    marker.pose.position.x = 0.0;
    marker.pose.position.y = 0.0;
    marker.pose.position.z = 0.0;
    marker.pose.orientation.x = 0.0;
    marker.pose.orientation.y = 0.0;
    marker.pose.orientation.z = 0.0;
    marker.pose.orientation.w = 1.0;
    marker.scale.x = 0.05;
    marker.scale.y = 0.05;
    marker.scale.z = 0.05;
    marker.color.a = 1.0; // Don't forget to set the alpha!
    marker.color.r = std::min(int(color_scale*(localizablity) + 0.5),255)/255.0;
    marker.color.g = 0.0;
    marker.color.b = std::max(255-int(color_scale*(localizablity) + 0.5), 0)/255.0;
    
    geometry_msgs::Point gp;
    gp.x = p.x;
    gp.y = p.y;
    gp.z = p.z;
    marker.points.push_back(gp);
    geometry_msgs::Point gp2;
    gp2.x = p.x + p_localizablity_dir.normal_x;
    gp2.y = p.y + p_localizablity_dir.normal_y;
    gp2.z = p.z + p_localizablity_dir.normal_z;
    marker.points.push_back(gp2);




    vis_pub.publish( marker );
    std_msgs::Float64 localizablity_msg;
    localizablity_msg.data = localizablity/10;
    localizablity_pub.publish(localizablity_msg);

    std_msgs::Float64 state_estimation_error_msg;
    state_estimation_error_msg.data = state_estimation_error;
    state_estimation_error_pub.publish(state_estimation_error_msg);

    std_msgs::Float64 inv_localizablity_msg;
    inv_localizablity_msg.data = 10/localizablity;
    inverse_localizablity_pub.publish(inv_localizablity_msg);


    std::cout << localizablity << std::endl;

  }


  // if(save_path.compare("") != 0){
  //   pcl::io::savePCDFileASCII(save_path, *localizablity_cloud);  
  // }
  
  // boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer (new pcl::visualization::PCLVisualizer ("3D Viewer"));
  // viewer->addPointCloud (cloud, "cloud"); // Method #1
  // // viewer->addPointCloud (boundary_cloud, "boundary_cloud");
  // // viewer->addPointCloud (cloud, "normals"); // Method #1
 
  // pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGB> rgb(localizablity_cloud);

  // viewer->addPointCloud<pcl::PointXYZRGB> (localizablity_cloud, rgb, "localizablity_cloud"); // Method #1
  // viewer->addPointCloudNormals<pcl::PointXYZRGB, pcl::Normal> (localizablity_cloud, localizablity_dirs, 1, 1, "localizablity_dirs");  
  // viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_COLOR, 0.0f, 1.0f, 0.0f, "localizablity_dirs"); 

  // if(show_normals>0){
  //   viewer->addPointCloudNormals<pcl::PointXYZ, pcl::Normal> (cloud, cloud_normals, show_normals, .25, "normals");  
  //   viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_COLOR, 1.0f, 0.0f, 0.0f, "normals"); 
  // }
  // // viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_COLOR, 0.0f, 1.0f, 0.0f, "boundary_cloud");
  // // viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_COLOR, 0.0f, 1.0f, 0.0f, "localizablity_cloud"); 
  // viewer->addCoordinateSystem(1.0, 0, 0, 0, 0); 
  // viewer->setBackgroundColor (0, 0, 0, 0);   // Setting background to a dark grey 
  // viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 1, "cloud"); 
  // // viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 10, "boundary_cloud"); 
  // viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 5, "localizablity_cloud"); 

  // viewer->initCameraParameters ();
  

  // while (!viewer->wasStopped ()) { 
  //     viewer->spinOnce (); 
  // } 


  

}


