/**
 * @file   localizability_interpolator.cpp
 * @author Sam Zeng
 * @date   11/17/2017
 *
 * @copyright
 * Copyright (C) 2017.
 */

#include <localizablity/Interpolator.h>


int main(int argc,  char** argv)
{
  ros::init(argc, argv, "localizablity_interpolator");
  ros::NodeHandle n;

  tf::TransformListener listener;
  ros::Publisher vis_pub = n.advertise<visualization_msgs::Marker>( "visualization_marker", 0 );

  ros::Publisher localizablity_pub = n.advertise<std_msgs::Float64>( "localizablity", 0 );
  ros::Publisher inverse_localizablity_pub = n.advertise<std_msgs::Float64>( "inverse_localizablity", 0 );
  ros::Publisher state_estimation_error_pub = n.advertise<std_msgs::Float64>( "state_estimation_error", 0 );
  ros::Rate loop_rate(10);

  float current_localizablity;
  float color_scale = 2500;

  Interpolator I = Interpolator();
  cout << "CREATED INTERPOLATOR" << endl;

  while(ros::ok()){
    pcl::PointXYZ p;
    tf::StampedTransform transform;
    try{
      listener.lookupTransform("/world", "/body",  
                               ros::Time(0), transform);
    }
    catch (tf::TransformException ex){
      ROS_ERROR("%s",ex.what());
      ros::Duration(1.0).sleep();
    }
    p.x = transform.getOrigin().x();
    p.y = transform.getOrigin().y();
    p.z = transform.getOrigin().z();
    
    // try{
    //   listener.lookupTransform("/body", "/body_1",  
    //                            ros::Time(0), transform);
    //   std_msgs::Float64 pose_err_msg;
    //   pose_err_msg.data = sqrt( pow(transform.getOrigin().x(),2) + pow(transform.getOrigin().y(),2) + pow(transform.getOrigin().z(),2) );
    //   state_estimation_error_pub.publish(pose_err_msg);
    // }
    // catch (tf::TransformException ex){
    //   ROS_ERROR("%s",ex.what());
    //   ros::Duration(1.0).sleep();
    // }


    current_localizablity = I.get_localizablity(p.x,p.y,p.z);

    std_msgs::Float64 localizablity_msg;
    localizablity_msg.data = current_localizablity/4;
    localizablity_pub.publish(localizablity_msg);
    localizablity_msg.data = 1/current_localizablity;	
    inverse_localizablity_pub.publish(localizablity_msg);



    visualization_msgs::Marker marker;
    marker.header.frame_id = "/world";
    marker.header.stamp = ros::Time();
    marker.ns = "my_namespace";
    marker.id = 0;
    marker.type = visualization_msgs::Marker::SPHERE;
    marker.action = visualization_msgs::Marker::ADD;
    marker.pose.position.x = p.x;
    marker.pose.position.y = p.y;
    marker.pose.position.z = p.z;
    marker.pose.orientation.x = 0.0;
    marker.pose.orientation.y = 0.0;
    marker.pose.orientation.z = 0.0;
    marker.pose.orientation.w = 1.0;
    marker.scale.x = 0.25;
    marker.scale.y = 0.25;
    marker.scale.z = 0.25;
    marker.color.a = 1.0; // Don't forget to set the alpha!
    marker.color.r = std::min(int(color_scale*(current_localizablity) + 0.5),255)/255.0;
    marker.color.g = 0.0;
    marker.color.b = std::max(255-int(color_scale*(current_localizablity) + 0.5), 0)/255.0;

    vis_pub.publish(marker);
    loop_rate.sleep();
  }

	return 0;
}