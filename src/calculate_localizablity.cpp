
/**
 * @file   calculate_localizability.cpp
 * @author Sam Zeng
 * @date   11/17/2017
 *
 * @copyright
 * Copyright (C) 2017.
 */

#include <localizablity/Calculate_Localizablity.h>


Calculate_Localizablity::Calculate_Localizablity(ros::NodeHandle n){

  resolution = .1;
  knn_radius = .75;
  color_scale = 2500;
  laser_range = 30;
  show_normals = 0;

  std::string save_path;

  std::string path_to_point_cloud;

  n.getParam("path_to_point_cloud", path_to_point_cloud);
  n.getParam("knn_radius", knn_radius);
  n.getParam("show_normals", show_normals);
  n.getParam("color_scale", color_scale);
  n.getParam("save_path", save_path);
  n.getParam("laser_range", laser_range);

  map_cloud = pcl::PointCloud<pcl::PointXYZ>().makeShared();
  normals = pcl::PointCloud<pcl::Normal>().makeShared();
  voxel_cloud = pcl::PointCloud<pcl::PointXYZ>().makeShared();
  voxel_normals = pcl::PointCloud<pcl::Normal>().makeShared();
  localizablity_cloud = pcl::PointCloud<pcl::PointXYZRGB>().makeShared();
  localizablity_dirs  = pcl::PointCloud<pcl::Normal>().makeShared();



  octree = new pcl::octree::OctreePointCloudSearch<pcl::PointXYZ>(resolution);
  voxel_octree = new pcl::octree::OctreePointCloudSearch<pcl::PointXYZ>(resolution);

  pcl::io::loadPCDFile<pcl::PointXYZ>(path_to_point_cloud, *map_cloud);
  Calculate_Localizablity::compute_octree();
  Calculate_Localizablity::compute_normals();
  Calculate_Localizablity::compute_voxel_normals();

  // Calculate_Localizablity::visualize();
}

void Calculate_Localizablity::visualize(){
  boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer (new pcl::visualization::PCLVisualizer ("3D Viewer"));
  viewer->addPointCloud (voxel_cloud, "cloud"); // Method #1

  pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGB> rgb(localizablity_cloud);
  viewer->addPointCloud<pcl::PointXYZRGB> (localizablity_cloud, rgb, "localizablity_cloud"); // Method #1

  if(show_normals>0){
    viewer->addPointCloudNormals<pcl::PointXYZ, pcl::Normal> (voxel_cloud, voxel_normals, show_normals, .25, "normals");  
    viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_COLOR, 1.0f, 0.0f, 0.0f, "normals"); 
  }

  // viewer->addPointCloudNormals<pcl::PointXYZRGB, pcl::Normal> (localizablity_cloud, localizablity_dirs, 1, 1, "localizablity_dirs");  
  // viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_COLOR, 0.0f, 1.0f, 0.0f, "localizablity_dirs");

  viewer->addCoordinateSystem(1.0, 0, 0, 0, 0); 
  viewer->setBackgroundColor (0, 0, 0, 0);   // Setting background to a dark grey 
  viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 1, "cloud"); 
  viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 20, "localizablity_cloud"); 

  viewer->initCameraParameters ();

  while (!viewer->wasStopped ()) { 
      viewer->spinOnce (); 
  } 

}

void Calculate_Localizablity::compute_octree(){  
  octree->setInputCloud(map_cloud);
  octree->addPointsFromInputCloud();
  ROS_INFO("BUILT OCTREE");
}

void Calculate_Localizablity::compute_normals(){
  pcl::NormalEstimation<pcl::PointXYZ, pcl::Normal> ne;
  ne.setInputCloud (map_cloud);

  pcl::search::KdTree<pcl::PointXYZ>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZ> ());
  ne.setSearchMethod (tree);

  ne.setRadiusSearch (knn_radius);
  ne.compute (*normals);
  ROS_INFO("COMPUTED NORMALS");
}

void Calculate_Localizablity::compute_voxel_normals(){
  std::vector<pcl::PointXYZ, Eigen::aligned_allocator<pcl::PointXYZ> > voxel_centroids;
  octree->getOccupiedVoxelCenters(voxel_centroids);
  

  bool valid_point;
  float nx;
  float ny;
  float nz;
  float K;
  int counter;
  int num_good_points;
  for(int i = 0; i < voxel_centroids.size(); ++i){
    std::vector< int > point_idx_data;
    valid_point = octree->voxelSearch(voxel_centroids[i],point_idx_data);
    if(!valid_point){
      continue;
    }
    
    nx = 0;
    ny = 0;
    nz = 0;
    K = 0;
    num_good_points = 0;
    counter = 0;
    for (int j = 0; j < point_idx_data.size(); ++j){
        if(isnan((normals->points[point_idx_data[j]]).normal_x)){
          counter++;
          // ROS_WARN("NAN normal value");
          continue;
        }
        // ROS_INFO("Normal Normals lol...");

        nx += (normals->points[point_idx_data[j]]).normal_x;
        ny += (normals->points[point_idx_data[j]]).normal_y;
        nz += (normals->points[point_idx_data[j]]).normal_z;
        K  += (normals->points[point_idx_data[j]]).curvature;
    }
    num_good_points = point_idx_data.size()-counter;
    if(num_good_points<=0){
      ROS_WARN("ALL POINTS BAD?");
      continue;
    }
    nx = nx/(num_good_points);
    ny = ny/(num_good_points);
    nz = nz/(num_good_points);
    K  = K/(num_good_points);
    pcl::PointXYZ p;
    pcl::Normal n;
    p.x = voxel_centroids[i].x;
    p.y = voxel_centroids[i].y;
    p.z = voxel_centroids[i].z;
    
    n.normal_x = nx;
    n.normal_y = ny;
    n.normal_z = nz;
    n.curvature = K;

    voxel_cloud->push_back(p);
    voxel_normals->push_back(n);

  }
  voxel_octree->setInputCloud(voxel_cloud);
  voxel_octree->addPointsFromInputCloud();
  ROS_INFO("BUILT VOXEL OCTREE");
}


float Calculate_Localizablity::estimate_localizablity(float x, float y, float z, float yaw){
  // float yaw = -M_PI/4;
  
  std::vector< int > intersectedVoxels;
  Eigen::Vector3f direction;
  Eigen::Vector3f origin = Eigen::Vector3f(x,y,z);
  double d_phi = 2.0;
  double d_theta = 2.0;
  double max_angle = 3*M_PI/4;
  Eigen::Matrix3f It(3,3);
  It.setZero();
  Eigen::Matrix3f Ir(3,3);
  Ir.setZero();
  Eigen::MatrixXf I(6,6);
  I.setZero();
  //fix this to take resolution arg correctly
  view_map map = view_map(d_phi);

  debug_cloud = pcl::PointCloud<pcl::PointXYZRGB>().makeShared();
  int counter = 1;

  for (double phi = 0; phi < 360; phi += d_phi){
    for (double theta = 0; theta < 180; theta += d_theta){
      direction = map.sphere_angles_to_vector(phi*M_PI/180,theta*M_PI/180,yaw);
      int count = voxel_octree->getIntersectedVoxelIndices(origin,direction,intersectedVoxels,1);

      // std::cout << angle*180/M_PI << " " << phi << " " << theta << std::endl;

        if(theta>120){
          continue;
        }
        // Eigen::Vector3f x_axis = Eigen::Vector3f(cos(yaw),sin(yaw),0);
        // float angle = atan2( direction.cross(x_axis).norm(), direction.dot(x_axis));
        // debug_pt.x = direction[0] + x;
        // debug_pt.y = direction[1] + y;
        // debug_pt.z = direction[2] + z; 
        // debug_pt.r = fabs(255*cos(angle));
        // debug_pt.g = 0.0;
        // debug_pt.b = fabs(255*sin(angle));
        // debug_cloud->push_back(debug_pt);


      if(count == 1){
        float distance = sqrt( pow(voxel_cloud->points[intersectedVoxels[0]].x - x,2) + pow(voxel_cloud->points[intersectedVoxels[0]].y - y,2) + pow(voxel_cloud->points[intersectedVoxels[0]].z - z,2));
        if(distance>laser_range){
          continue;
        }

        counter++;
      
        Eigen::Vector3f x_axis = Eigen::Vector3f(cos(yaw),sin(yaw),0);
        Eigen::Vector3f n;
        n[0] = voxel_normals->points[intersectedVoxels[0]].normal_x;
        n[1] = voxel_normals->points[intersectedVoxels[0]].normal_y;
        n[2] = voxel_normals->points[intersectedVoxels[0]].normal_z;
        float cos_angle = (n.dot(-direction));
        float angle = acos(cos_angle);

        // std::cout << 360*fabs(angle)/(2*M_PI) << std::endl;
        if( 360*fabs(angle)/(2*M_PI) > 75.0 ){
          continue;
        }
        // float angle = atan2( direction.cross(x_axis).norm(), direction.dot(x_axis));

        // float scale = 2;
        pcl::PointXYZRGB debug_pt;
        debug_pt.x = direction[0] + x;
        debug_pt.y = direction[1] + y;
        debug_pt.z = direction[2] + z; 
        // debug_pt.r = fabs(255*cos(angle));
        // debug_pt.g = 0.0;
        // debug_pt.b = fabs(255*sin(angle));

        // debug_pt.x = voxel_cloud->points[intersectedVoxels[0]].x;
        // debug_pt.y = voxel_cloud->points[intersectedVoxels[0]].y;
        // debug_pt.z = voxel_cloud->points[intersectedVoxels[0]].z; 
        debug_pt.r = (128+(127*(voxel_normals->points[intersectedVoxels[0]]).normal_x));
        debug_pt.g = (128+(127*(voxel_normals->points[intersectedVoxels[0]]).normal_y));
        debug_pt.b = (128+(127*(voxel_normals->points[intersectedVoxels[0]]).normal_z));
        debug_cloud->push_back(debug_pt);



        // float normal2ray_cos = (n[0]*direction[0]  + n[1]*direction[1]);

        // Eigen::Matrix3f R;
        // R = Eigen::Quaternionf().setFromTwoVectors(x_axis,-n);
        // Eigen::Vector3f ri = direction;
        // Eigen::Vector3f A (0, acos(ri[0]/(sqrt(pow(ri[0],2) + pow(ri[2],2)))),  acos(ri[0]/(sqrt(pow(ri[0],2) + pow(ri[1],2)))));
        // Eigen::Vector3f p0(voxel_cloud->points[intersectedVoxels[0]].x,voxel_cloud->points[intersectedVoxels[0]].y,voxel_cloud->points[intersectedVoxels[0]].z );
        // float d = (n).dot(-p0);
        // Eigen::Vector3f dr_dth(0, d*tan(A[1])/cos(A[1]), d*tan(A[2])/cos(A[2]));
        // A = R*A;
        // Eigen::Vector3f dr_dx = (A.array().cos());
        // dr_dx = R.inverse()*dr_dx;
        // std::cout << dr_dx.transpose() << std::endl;
        
        // dr_dx[0] = d/dr_dx[0];
        // dr_dx[1] = d/dr_dx[1];
        // dr_dx[2] = d/dr_dx[2];
        // std::cout << dr_dx.transpose() << std::endl;

        // map.add_point(phi,theta,voxel_cloud->points[intersectedVoxels[0]],voxel_normals->points[intersectedVoxels[0]],angle);
        It = It + n*n.transpose()/pow(cos_angle,2);
        // Ir = Ir + dr_dth*dr_dth.transpose();
        // It = It + dr_dx*dr_dx.transpose();

      }

    
    }
  }
  // std::cout << It << std::endl;
  Eigen::JacobiSVD<Eigen::MatrixXf> svd_test(It, Eigen::ComputeThinU | Eigen::ComputeThinV);
  
  // std::cout << debug_cloud->size() << std::endl;
  // map.normal_data = map.normal_data.cwiseProduct(map.density_mask);
  // map.normal_data = map.normal_data.cwiseProduct(map.curvature_data);
  // map.normal_data.conservativeResize(counter,3);

  // Eigen::JacobiSVD<Eigen::MatrixXf> svd(map.normal_data, Eigen::ComputeThinU | Eigen::ComputeThinV);
  Eigen::MatrixXf::Index minRow, minCol;
  float min = svd_test.singularValues().minCoeff(&minRow, &minCol);
  float L = sqrt(svd_test.singularValues().minCoeff());
  // float L = (map.normal_data).colwise().norm().minCoeff();
  L = 300*L/(360.0*180.0/(d_phi*d_theta));
  // std::cout << L << std::endl;

  // std::cout << (map.normal_data).colwise().norm() << std::endl;
  // std::cout << map.normal_data << std::endl;
  pcl::PointXYZRGB L_pt;
  pcl::Normal L_n;
  L_pt.x = x;
  L_pt.y = y;
  L_pt.z = z;
  L_pt.r = std::min(int(color_scale*(L)),255);
  L_pt.g = 0;
  L_pt.b = std::max(255-int(color_scale*(L)), 0);
  
  L_n.normal_x = (svd_test.matrixV()(0,minRow));
  L_n.normal_y = (svd_test.matrixV()(1,minRow));
  L_n.normal_z = (svd_test.matrixV()(2,minRow));

  localizablity_cloud->push_back(L_pt);
  localizablity_dirs->push_back(L_n);

  // Calculate_Localizablity::debug_visualize();

  if(isinf(L) || isnan(L)){
    return 0;
  }
  return L;
}

void Calculate_Localizablity::debug_visualize(){
  boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer (new pcl::visualization::PCLVisualizer ("3D Viewer"));
  viewer->addPointCloud (voxel_cloud, "cloud"); // Method #1

  pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGB> rgb(debug_cloud);
  viewer->addPointCloud<pcl::PointXYZRGB> (debug_cloud, rgb, "localizablity_cloud"); // Method #1

  if(show_normals>0){
    viewer->addPointCloudNormals<pcl::PointXYZ, pcl::Normal> (voxel_cloud, voxel_normals, show_normals, .25, "normals");  
    viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_COLOR, 1.0f, 0.0f, 0.0f, "normals"); 
  }

  viewer->addCoordinateSystem(1.0, 0, 0, 0, 0); 
  viewer->setBackgroundColor (0, 0, 0, 0);   // Setting background to a dark grey 
  viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 1, "cloud"); 
  viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 4, "localizablity_cloud"); 

  viewer->initCameraParameters ();

  while (!viewer->wasStopped ()) { 
      viewer->spinOnce (); 
  } 

}