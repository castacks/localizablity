/**
 * @file   interpolator.cpp
 * @author Sam Zeng
 * @date   11/17/2017
 *
 * @copyright
 * Copyright (C) 2017.
 */


#include <localizablity/Interpolator.h>
#include <ros/package.h>

using namespace std;


// assumes data is monotonic and increasing
// returns index corresponding to input point 
float Interpolator::interp_1d(float data[], int length,float q_pt){
	if(q_pt <= data[0]){
		return 0;
	}else if(q_pt >= data[length-1]){
		return length-1;
	}
	for(int i = 1; i<length; ++i){
		if(q_pt <= data[i]){
			return i-(data[i]-q_pt)*(1)/(data[i]-data[i-1]);
		}
	}

}


// q pts are indicies
float Interpolator::interp_3d(float q_x, float q_y, float q_z){

	int x0 = int(q_x);
	int x1 = x0 + 1;

	int y0 = int(q_y);
	int y1 = y0 + 1;
	
	int z0 = int(q_z);
	int z1 = z0 + 1;
	
	if(x1 >= length){
		x0 = length - 2; 
		x1 = length - 1;
		q_x = length - 1;  
	}


	if(y1 >= width){
		y0 = width - 2; 
		y1 = width - 1;
		q_y = width - 1;  
	}


	if(z1 >= height){
		z0 = height - 2; 
		z1 = height - 1;
		q_z = height - 1;  
	}

	float x_d = (q_x-x0)/(x1-x0);
	float y_d = (q_y-y0)/(y1-y0);
	float z_d = (q_z-z0)/(z1-z0);


	float c_00 = L[x0][y0][z0]*(1 - x_d) + L[x1][y0][z0]*x_d;
	float c_01 = L[x0][y0][z1]*(1 - x_d) + L[x1][y0][z1]*x_d;
	float c_10 = L[x0][y1][z0]*(1 - x_d) + L[x1][y1][z0]*x_d;
	float c_11 = L[x0][y1][z1]*(1 - x_d) + L[x1][y1][z1]*x_d;

	float c_0 = c_00*(1 - y_d) + c_10*y_d;
	float c_1 = c_01*(1 - y_d) + c_11*y_d;

	float c = c_0*(1 - z_d) + c_1*z_d;

	return c;

}

float Interpolator::get_localizablity(float x, float y, float z)
{


  float x_idx = interp_1d(X,length,x);
  float y_idx = interp_1d(Y,width,y);
  float z_idx = interp_1d(Z,height,z);
  float localizablity = Interpolator::interp_3d(x_idx,y_idx,z_idx);
  return localizablity;
}


// Interpolator::Interpolator(ros::NodeHandle n)
Interpolator::Interpolator()
{

  string path = ros::package::getPath("localizablity");
  ifstream L_file ((path+"/data/L.txt").c_str());
  if(!L_file.is_open()){
  	cout << "Unable to open file" << endl;
  }
  string line;
  getline(L_file,line);
  length = atof(line.c_str());
  getline(L_file,line);
  width = atof(line.c_str());
  getline(L_file,line);
  height = atof(line.c_str());

  X = new float [length];
  Y = new float [width];
  Z = new float [height];


  L = new float**[length];
  for (int i = 0; i < length; ++i){
  	L[i] = new float*[width];
  	for(int j = 0; j < width; ++j){
  		L[i][j] = new float[height];
  	}
  }

  // float L [length][width][height];


  // pcl::PointCloud<pcl::PointXYZRGB>::Ptr base_cloud (new pcl::PointCloud<pcl::PointXYZRGB>);

  std::cout << length << " " << width << " " << height << std::endl;

  for (int i = 0; i < length; ++i){
		for (int j = 0; j < width; ++j){
			for (int k = 0; k < height; ++k){
				getline(L_file,line,' ');
				float x = atof(line.c_str());
				X[i] = x;
				getline(L_file,line,' ');
				float y = atof(line.c_str());
				Y[j] = y;
				getline(L_file,line,' ');
				float z = atof(line.c_str());
				Z[k] = z;
				getline(L_file,line,'\n');
				float l = atof(line.c_str());

				L[i][j][k] = l;

  			// pcl::PointXYZRGB p;
  			// p.x = x;
  			// p.y = y;
  			// p.z = z;
  			// p.r =  std::min(int(color_scale*(l) + 0.5),255);
     //    p.g = 0;
     //    p.b = std::max(255-int(color_scale*(l) + 0.5), 0);
     //    base_cloud->push_back(p);

			}
		}
  }

}

void Interpolator::level_set(float threshold, pcl::PointCloud<pcl::PointXYZ>::Ptr boundary_cloud){
  
  float dx = .2;
  float dy = .2;
  float dz = .2;
  
  float x_min = X[0];
  float x_max = X[length-1];

  float y_min = Y[0];
  float y_max = Y[width-1];

  float z_min = Z[0];
  float z_max = Z[height-1];

  for(float x = x_min; x <= x_max; x+=dx){
    for(float y = y_min; y <= y_max; y+=dy){
      for(float z = z_min; z <= z_max; z+=dz){
        float localizablity = get_localizablity(x,y,z);
        // std::cout << localizablity << std::endl;
        if(localizablity > threshold && !isnan(localizablity) && !isinf(localizablity)){
          // for(int i_n=-1; i_n<=1; ++i_n){
          //   for(int j_n=-1;j_n<=1; ++j_n){
          //     for(int k_n=-1;k_n<=1;++k_n){
          //       float q_x = x + (dx*i_n);
          //       float q_y = y + (dy*j_n);
          //       float q_z = z + (dz*k_n);
                
          //       if(((q_x)>=x_min && (q_x)<x_max) && ((q_y)>=y_min && (q_y)<y_max) && ((q_z)>=z_min && (q_z)<z_max)){
          //         // std::cout << i+i_n << " " << j+j_n << " " << k+k_n << std::endl;
          //         if(get_localizablity(q_x,q_y,q_z)<threshold){
          pcl::PointXYZ p;
          p.x = x;
          p.y = y;
          p.z = z;
          boundary_cloud->push_back(p);
          // i_n = 2;
          // j_n = 2;
          // k_n = 2;
          //         }  
          //       }
          //     }
          //   }
          // }
        }
      }
    }
  }
}
  // boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer (new pcl::visualization::PCLVisualizer ("3D Viewer"));
 
  // pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGB> base_rgb(base_cloud);

  // viewer->addPointCloud<pcl::PointXYZRGB> (base_cloud, base_rgb, "base_cloud"); // Method #1

  // viewer->addCoordinateSystem(1.0, 0, 0, 0, 0); 
  // viewer->setBackgroundColor (0, 0, 0, 0);
  // viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 1, "base_cloud"); 

  // viewer->initCameraParameters ();
  



  // pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZRGB>);

  // for (float x = X[0]; x < X[length-1]; x+=0.25){
  // 	for (float y = Y[0]; y < Y[width-1]; y+=0.25){
  // 		for (float z = Z[0]; z < Z[height-1]; z+=0.25){

  // 			pcl::PointXYZRGB p;
  // 			p.x = x;
  // 			p.y = y;
  // 			p.z = z;
		//     float x_idx = interp_1d(X,length,p.x);
		// 		float y_idx = interp_1d(Y,width,p.y);
		//     float z_idx = interp_1d(Z,height,p.z);

  // 			current_localizablity = interp_3d(L,length,width,height,x_idx,y_idx,z_idx);
  // 			p.r =  std::min(int(color_scale*(current_localizablity) + 0.5),255);
  //       p.g = 0;
  //       p.b = std::max(255-int(color_scale*(current_localizablity) + 0.5), 0);
  //       cloud->push_back(p);
  // 		}
  // 	}
  // }



  // boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer_2 (new pcl::visualization::PCLVisualizer ("3D Viewer_2"));
 
  // pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGB> rgb(cloud);

  // viewer_2->addPointCloud<pcl::PointXYZRGB> (cloud, rgb, "cloud"); // Method #1

  // viewer_2->addCoordinateSystem(1.0, 0, 0, 0, 0); 
  // viewer_2->setBackgroundColor (0, 0, 0, 0);
  // viewer_2->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 1, "cloud"); 

  // viewer_2->initCameraParameters ();
  

  // while (!viewer_2->wasStopped ()) { 
  //     viewer_2->spinOnce (); 
  // } 

  // while (!viewer->wasStopped ()) { 
  //     viewer->spinOnce (); 
  // } 
