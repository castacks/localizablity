/**
 * @file   view_map.h
 * @author Sam Zeng
 * @date   11/17/2017
 *
 * @copyright
 * Copyright (C) 2017.
 */


#ifndef VIEW_MAP_H
#define VIEW_MAP_H

#include <Eigen/Core>
#include <math.h> 
#include <pcl/point_cloud.h>
#include <pcl/io/pcd_io.h>
#include <ros/ros.h>


class view_map
{
public:
	float resolution;
	int length;
	int count;
	Eigen::MatrixX3f normal_data;
	Eigen::MatrixX3f curvature_data;
	Eigen::MatrixX3f density_mask;

	
	// Eigen::MatrixXf 

	view_map(float res);
	void add_point(double phi, double theta, pcl::PointXYZ,pcl::Normal, double density_angle);
	void generate_density_mask();
	// ~view_map();


	// Theta: inclination [0,pi]
	// Phi: azimuth [0,2pi]
	// Unit sphere: r = 1
	Eigen::Vector3f sphere_angles_to_vector(float, float, float);
};
#endif

