/**
 * @file   Calculate_Localizability.cpp
 * @author Sam Zeng
 * @date   11/17/2017
 *
 * @copyright
 * Copyright (C) 2017.
 */


#ifndef CALCULATE_LOCALIZABLITY_H
#define CALCULATE_LOCALIZABLITY_H

#include <ros/ros.h>
#include <pcl/point_cloud.h>
#include <pcl/octree/octree.h>
#include <pcl/io/pcd_io.h>
#include <iostream>
#include <pcl/features/normal_3d.h>
#include <pcl/visualization/cloud_viewer.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <math.h> 
#include <string>
#include <boost/thread/thread.hpp>
#include "view_map.h"
#include <fstream>




class Calculate_Localizablity
{
public:
  pcl::PointCloud<pcl::PointXYZ>::Ptr map_cloud;
  pcl::PointCloud<pcl::Normal>::Ptr normals;

  pcl::PointCloud<pcl::PointXYZ>::Ptr voxel_cloud;
  pcl::PointCloud<pcl::Normal>::Ptr voxel_normals;

  pcl::PointCloud<pcl::PointXYZRGB>::Ptr localizablity_cloud;
  pcl::PointCloud<pcl::Normal>::Ptr localizablity_dirs;

  
  pcl::octree::OctreePointCloudSearch<pcl::PointXYZ> *octree;
  pcl::octree::OctreePointCloudSearch<pcl::PointXYZ> *voxel_octree;

  pcl::PointCloud<pcl::PointXYZRGB>::Ptr debug_cloud;

  float resolution;
  float knn_radius;
  float color_scale;
  float laser_range;
  int show_normals;


  Calculate_Localizablity(ros::NodeHandle);
  void compute_octree();
  void compute_normals();
  void compute_voxel_normals();
  void visualize();
  void debug_visualize();
  float estimate_localizablity(float x, float y, float z, float yaw= -M_PI/4);
  // float estimate_localizablity(float x, float y, float z);

};
#endif
