/**
 * @file   Interpolator.h
 * @author Sam Zeng
 * @date   11/17/2017
 *
 * @copyright
 * Copyright (C) 2017.
 */

#ifndef Interpolator_H
#define Interpolator_H

#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <ros/ros.h>
#include <visualization_msgs/Marker.h>
#include <tf/transform_listener.h>
#include <geometry_msgs/Point.h>
#include <std_msgs/Float64.h>
#include <pcl/common/common_headers.h>
#include <boost/thread/thread.hpp>
#include <pcl/visualization/pcl_visualizer.h>


class Interpolator
{
public:
  float ***L;
  int length;
  int width;
  int height;
  float *X; 
  float *Y;
  float *Z;

  // Interpolator(ros::NodeHandle n);
  Interpolator();
  float interp_1d(float data[], int length,float q_pt);
  float interp_3d(float q_x, float q_y, float q_z);
  float get_localizablity(float x, float y, float z);
  void level_set(float threshold, pcl::PointCloud<pcl::PointXYZ>::Ptr boundary_cloud);
};
#endif
